module Midnight
  module BusinessLogic
    module EventTest
      class TestStubEvent < Event
        class << self
          def backdoor_ensure_schema(data)
            ensure_schema(data)
          end
        end
      end

      class TestSchemaViolation < ::ArgumentError
        include Event::SchemaViolation
      end

      class TestEvent < Event
        class << self
          private

          def ensure_schema(data)
            case data[:case]
            when 0
              TestStubEvent.backdoor_ensure_schema(data)
            when 1
              'valid'
            when 2
              raise TestSchemaViolation
            else
              raise ArgumentError
            end
          end
        end
      end
      RSpec.describe Event do
        subject do
          TestEvent
        end

        it 'should accept data parameters from the consumer' do
          event = subject.new(data: {
            case: 1
          })
          expect(event.data[:case]).to eq(1)
        end

        it 'should validate schema violation when strictly construct the instance' do
          expect do
            subject.strict(data: {
              case: 2
            })
          end.to raise_error(Event::SchemaViolation)
        end

        it 'should not validate schema violation when construct the instance with the constructor' do
          expect do
            subject.new(data: {
              case: 2
            })
          end.not_to raise_error
        end

        it 'should construct instance properly when the schema is valid' do
          expect do
            subject.strict(data: {
              case: 1
            })
          end.not_to raise_error
        end

        it 'should declare stub method for data schema check' do
          expect do
            subject.strict(data: {
              case: 0
            })
          end.to raise_error(NotImplementedError)
        end

        it 'should accept consumer supplied metadata' do
          event = subject.strict(
            data: {
              case: 1,
            },
            metadata: Event::DEFAULT_METADATA_GENERATOR.call.merge(
              client: 'rspec'
            )
          )
          expect(event.metadata[:client]).to eq('rspec')
        end
      end

      RSpec.describe "#{Event.name}::DEFAULT_METADATA_GENERATOR" do
        subject do
          Event::DEFAULT_METADATA_GENERATOR
        end

        it 'should generate :occurred_at timestamp as string' do
          metadata = subject.call
          expect(metadata.keys).to contain_exactly(:occurred_at)
          expect do
            ::Time.parse(metadata[:occurred_at])
          end.not_to raise_error
        end
      end
    end
  end
end
