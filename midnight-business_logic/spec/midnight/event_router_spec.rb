module Midnight
  module BusinessLogic
    module EventRouterTest
      RSpec.describe EventRouter do
        EventA = Class.new
        EventB = Class.new
        EventC = Class.new

        let(:tracker) do
          {
            queue_a: [],
            queue_b: [],
          }
        end

        context do
          subject do
            described_class.new(
              EventA => [
                lambda do |event|
                  tracker[:queue_a] << event
                end
              ],
              EventB => lambda do |event|
                tracker[:queue_b] << event
              end
            )
          end

          it 'should be able to reject invalid dispatchers' do
            expect do
              described_class.new(1)
            end.to raise_error(::ArgumentError)
            expect do
              described_class.new({ a: 1 })
            end.to raise_error(::ArgumentError)
            expect do
              # valid
              described_class.new({
                a: lambda do |event|
                  event
                end
              })
            end.not_to raise_error
            expect do
              # valid
              described_class.new({
                ::ArgumentError => [
                  lambda do |event|
                    event
                  end
                ]
              })
            end.not_to raise_error
          end

          it 'should be able to route events to different dispatchers' do
            expect do
              subject.call(EventA.new)
              subject.call(EventB.new)
            end.not_to raise_error
            expect(tracker[:queue_a].map(&:class)).to eq([EventA])
            expect(tracker[:queue_b].map(&:class)).to eq([EventB])
          end

          it 'should ignore events without dispatchers' do
            expect do
              subject.call(EventC.new)
            end.not_to raise_error
            expect(tracker[:queue_a].size).to eq(0)
            expect(tracker[:queue_b].size).to eq(0)
          end

        end
      end
    end
  end
end
