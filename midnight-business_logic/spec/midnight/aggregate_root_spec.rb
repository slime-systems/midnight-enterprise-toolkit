module Midnight
  module BusinessLogic
    module AggregateRootTest
      CommandA = Class.new
      CommandB = Class.new
      EventA = Class.new
      EventB = Class.new
      TestBaseImplementationCommand = Class.new
      AllowCommandAOnlyOnce = Class.new(StandardError)

      class TestAggregate
        include AggregateRoot

        def initialize(state: nil)
          @state = state || {
            event_a_occurred: false
          }
        end

        def dispatch(command)
          case command
          when CommandA
            command_a_handler
          when CommandB
            command_b_handler
          when TestBaseImplementationCommand
            super(command)
          else
            raise ArgumentError, 'Unrecognized command'
          end
        end

        def command_a_handler
          raise AllowCommandAOnlyOnce if state[:event_a_occurred]
          apply(
            EventA.new
          )
        end

        def command_b_handler
          apply(
            EventB.new
          )
        end

        on EventA do |_event|
          state[:event_a_occurred] = true
        end
      end

      RSpec.describe AggregateRoot do
        subject do
          TestAggregate.new
        end

        it 'should handles a simple command dispatching' do
          expect do
            subject.dispatch(CommandA.new)
          end.not_to raise_error
        end

        it 'should apply events properly' do
          subject.dispatch(CommandA.new)
          event_class_list = subject.pending_events.map(&:class)
          expect(event_class_list).to contain_exactly(EventA)
          expect do
            subject.dispatch(CommandA.new)
          end.to raise_error(AllowCommandAOnlyOnce)
          expect(event_class_list).to contain_exactly(EventA)
        end

        it 'should be able to handle multiple command dispatching' do
          3.times do
            subject.dispatch(CommandB.new)
          end
          event_class_list = subject.pending_events.map(&:class)
          expect(event_class_list).to contain_exactly(*([EventB] * 3))
        end

        it 'should provides a stub method as a dispatch interface' do
          expect do
            subject.dispatch(TestBaseImplementationCommand.new)
          end.to raise_error(AggregateRoot::UnknownCommand)
        end
      end
    end
  end
end
