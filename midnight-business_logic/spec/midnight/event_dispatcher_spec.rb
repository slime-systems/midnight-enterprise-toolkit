module Midnight
  module BusinessLogic
    module EventDispatcherTest
      RSpec.describe EventDispatcher do
        EventA = Class.new

        let(:tracker) { [] }

        context do
          subject do
            described_class.new(
              lambda do |event|
                tracker << event
              end,
              lambda do |_event, state_id: nil|
                tracker << state_id if state_id
              end,
            )
          end

          it 'should not allow construction without an actual dispatchers' do
            expect do
              described_class.new(1)
            end.to raise_error(::ArgumentError)
          end

          it 'should allow propagation of ArgumentError' do
            expect do
              described_class.new(lambda do |_event|
                raise ::ArgumentError
              end).call(EventA.new)
            end.to raise_error(::ArgumentError)
          end

          it 'should be able to dispatch an event' do
            expect do
              subject.call(EventA.new)
            end.not_to raise_error
            expect(tracker.map(&:class)).to contain_exactly(\
              EventA,
            )
          end

          it 'should be able to dispatch an event with metadata' do
            expect do
              subject.call(EventA.new, state_id: :id)
            end.not_to raise_error
            expect(tracker.size).to eq(2)
            expect(tracker).to include(:id)
          end

          it 'should agnostic to weather the argument is a collection or not' do
            expect do
              subject.call([
                EventA.new
              ])
            end.not_to raise_error
            expect(tracker.first.map(&:class)).to contain_exactly(\
              EventA
            )
          end
        end

        context do
          subject do
            dispatcher_a = described_class.new(
              lambda do |event|
                tracker << event
              end
            )
            dispatcher_b = described_class.new(
              lambda do |event|
                tracker << event
              end
            )
            described_class.new(
              dispatcher_a,
              dispatcher_b
            )
          end

          it 'should be able to broadcast events to its children' do
            expect do
              subject.call(EventA.new)
            end.not_to raise_error
            expect(tracker.map(&:class)).to eq([
              EventA,
              EventA
            ])
          end
        end

        context do
          subject do
            dispatcher_a = described_class.new(
              lambda do |_event|
                tracker << :a
              end
            )
            dispatcher_b = described_class.new(
              lambda do |_event|
                tracker << :b
              end
            )
            described_class.new(
              dispatcher_b,
              dispatcher_a
            )
          end

          it 'should dispatch events in order' do
            expect do
              subject.call(EventA.new)
            end.not_to raise_error
            expect(tracker).to eq([
              :b,
              :a
            ])
          end
        end
      end
    end
  end
end
