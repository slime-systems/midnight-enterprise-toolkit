module Midnight
  module BusinessLogic
    module EventQueueTest
      RSpec.describe EventQueue do
        EventA = Class.new

        let(:tracker) { { call_count: 0, events: [] } }

        context do
          subject do
            described_class.new(
              lambda do |event|
                tracker[:call_count] += 1
                tracker[:events] << event
              end
            )
          end

          it 'should not allow construction without an actual dispatchers' do
            expect do
              described_class.new(1)
            end.to raise_error(::ArgumentError)
          end

          it 'should be able to break down a collection of events' do
            expect do
              subject.call([EventA.new, EventA.new])
            end.not_to raise_error
            expect(tracker[:events].map(&:class)).to contain_exactly(\
              EventA,
              EventA,
            )
            expect(tracker[:call_count]).to eq(2)
          end
        end
      end
    end
  end
end
