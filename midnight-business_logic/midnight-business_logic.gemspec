# frozen_string_literal: true

version = File.read(File.expand_path('../GEM_VERSION', __dir__)).strip
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'midnight/business_logic/version'

Gem::Specification.new do |spec|
  spec.name = 'midnight-business_logic'
  spec.version = version
  spec.author = 'Sarun Rattanasiri'
  spec.email = 'midnight_w@gmx.tw'
  spec.license = 'BSD-3-Clause'

  spec.summary = 'A toolkit for writing enterprise business logic'
  spec.description = 'Midnight::BusinessLogic is a toolkit for writing clean and robust business logics '\
    'agnostic to frameworks, user interfaces, and data storage. '\
    'Make the business logic fully testable '\
    'and provides isolation from external side effects.'
  spec.homepage = 'https://gitlab.com/slime-systems/midnight-enterprise-toolkit/tree/master/midnight-business_logic'
  spec.metadata = {
    'source_code_uri' => "https://gitlab.com/slime-systems/midnight-enterprise-toolkit/tree/v#{version}/midnight-business_logic"
  }

  spec.required_ruby_version = '>= 2.5.0'
  spec.files = Dir['lib/**/*']
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport'
end
