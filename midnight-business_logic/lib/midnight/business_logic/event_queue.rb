# frozen_string_literal: true

module Midnight
  module BusinessLogic
    class EventQueue < EventDispatcher
      def call(events, **metadata)
        events.each do |event|
          super(event, **metadata)
        end
      end
    end
  end
end
