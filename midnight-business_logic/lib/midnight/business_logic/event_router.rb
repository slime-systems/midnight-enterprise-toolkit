# frozen_string_literal: true

require 'active_support/core_ext/object/blank'

module Midnight
  module BusinessLogic
    class EventRouter < EventDispatcher
      def initialize(handlers_map = {})
        # construction example:
        # EventRouter.new(SomeEvent => [
        #   method(:puts)
        # ])
        begin
          handlers_map.each do |_event_class, handlers|
            Array(handlers).each do |handler|
              reject_handlers unless handler.respond_to?(:call)
            end
          end
        rescue ::NoMethodError => e
          raise e unless e.name == :each
          reject_handlers
        end
        @handlers_map = handlers_map
        @arity_mismatched = /wrong\s+number\s+of\s+arguments/i
        freeze
      end

      def call(event, **metadata)
        handlers = @handlers_map[event.class]
        return if handlers.blank?
        super(
          event,
          _event_handlers: Array(handlers),
          **metadata
        )
      end
    end
  end
end
