# frozen_string_literal: true

module Midnight
  module BusinessLogic
    class EventDispatcher
      def initialize(*handlers)
        handlers = handlers.map do |handler|
          if handler.respond_to?(:arity)
            handler
          else
            handler.method(:call)
          end
        rescue ::NameError
          reject_handlers
        end
        @handlers = handlers
        @arity_mismatched = /wrong\s+number\s+of\s+arguments/i
        @keywords_mismatched = /unknown\s+keywords/i
        freeze
      end

      def call(\
        events,
        # consumers are forbidden
        # to pass metadata with _event_handlers as a key
        _event_handlers: @handlers,
        **metadata
      )
        _event_handlers.each do |handler|
          handler.call(events, **metadata)
        rescue ::ArgumentError => e
          if [
            @arity_mismatched,
            @keywords_mismatched,
          ].any? do |pattern|
            pattern =~ e.message
          end
            handler.call(events)
          else
            raise e
          end
        end
      end

      private

      def reject_handlers
        raise ::ArgumentError, 'Invalid event handler'
      end
    end
  end
end
