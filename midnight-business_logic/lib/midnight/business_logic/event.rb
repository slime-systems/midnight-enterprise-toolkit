# frozen_string_literal: true

require 'time'
require 'active_support/core_ext/time'

module Midnight
  module BusinessLogic
    class Event
      module SchemaViolation
      end

      DEFAULT_METADATA_GENERATOR = lambda do
        {
          occurred_at: ::Time.current.iso8601(6)
        }
      end

      def initialize(data: {}, metadata: nil)
        @data = data
        @metadata = metadata || DEFAULT_METADATA_GENERATOR.call
        freeze
      end

      attr_reader :data, :metadata

      class << self
        def strict(data: {}, metadata: nil)
          new(
            data: ensure_schema(data),
            metadata: metadata
          )
        end

        private

        def ensure_schema(_data)
          raise NotImplementedError
          # example
          # @data_schema ||= ::Dry::Schema.Params do
          #   required(:example_field).filled(:string)
          # end
          # result = @data_schema.call(data)
          # raise SchemaViolation unless result.success?
        end
      end
    end
  end
end
