# frozen_string_literal: true

require 'active_support/concern'

module Midnight
  module BusinessLogic
    module AggregateRoot
      class UnknownCommand < ArgumentError
      end

      extend ::ActiveSupport::Concern

      def dispatch(_command)
        raise UnknownCommand
      end

      def state
        @state ||= {}
      end

      def pending_events
        @pending_events ||= []
      end

      class_methods do
        attr_accessor :_event_handlers

        private

        def on(event_class, &block)
          self._event_handlers ||= {}
          self._event_handlers[event_class] = block
        end
      end

      private

      def apply(*events)
        handlers = self.class._event_handlers || {}
        events.each do |event|
          handler = handlers[event.class]
          instance_exec(event, &handler) if handler
          pending_events << event
        end
      end
    end
  end
end
