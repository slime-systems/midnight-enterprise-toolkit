# frozen_string_literal: true

module Midnight
  module Commons
    NULL_EVENT_HANDLER = lambda { |*_args| }
  end
end
