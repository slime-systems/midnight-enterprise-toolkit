# frozen_string_literal: true

# just a convenient marker
module Midnight
  module Commons
    module ConcurrentWriteError
    end
  end
end
