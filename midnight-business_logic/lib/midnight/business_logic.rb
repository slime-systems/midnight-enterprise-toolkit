# frozen_string_literal: true

require 'active_support/dependencies/autoload'
require_relative 'business_logic/version'

module Midnight
  module Commons
    extend ::ActiveSupport::Autoload

    autoload :ConcurrentWriteError
    autoload :NULL_EVENT_HANDLER, 'midnight/commons/constants'
  end

  module BusinessLogic
    extend ::ActiveSupport::Autoload

    autoload :AggregateRoot
    autoload :Event
    autoload :EventDispatcher
    autoload :EventQueue
    autoload :EventRouter
  end
end
