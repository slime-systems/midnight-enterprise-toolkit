# frozen_string_literal: true

require 'active_support/core_ext/hash'
require 'json'

module Midnight
  module Utilities
    module JsonSerializer
      module_function def dump(hash)
        hash = hash.try(:to_hash) || hash
        ::JSON.dump(hash)
      rescue ::TypeError
        hash
      end

      module_function def load(string)
        hash = ::JSON.load(string)
        hash.try(:to_hash).try(:deep_symbolize_keys) || hash
      rescue ::TypeError
        string
      end
    end
  end
end
