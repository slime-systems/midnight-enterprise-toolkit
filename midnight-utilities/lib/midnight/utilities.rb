# frozen_string_literal: true

require_relative 'utilities/version'
require 'active_support/dependencies/autoload'

module Midnight
  module Utilities
    extend ::ActiveSupport::Autoload
    autoload :JsonSerializer
  end
end
