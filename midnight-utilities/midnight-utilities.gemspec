# frozen_string_literal: true

version = File.read(File.expand_path('../GEM_VERSION', __dir__)).strip
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'midnight/utilities/version'

Gem::Specification.new do |spec|
  spec.name = 'midnight-utilities'
  spec.version = version
  spec.author = 'Sarun Rattanasiri'
  spec.email = 'midnight_w@gmx.tw'
  spec.license = 'BSD-3-Clause'

  spec.summary = 'A reusable utilities pool supporting Midnight::Rails'
  spec.description = 'A collection of potentially reusable elements extracted from midnight-rails gem.'
  spec.homepage = 'https://gitlab.com/slime-systems/midnight-enterprise-toolkit/tree/master/midnight-utilities'
  spec.metadata = {
    'source_code_uri' => "https://gitlab.com/slime-systems/midnight-enterprise-toolkit/tree/v#{version}/midnight-utilities"
  }

  spec.required_ruby_version = '>= 2.5.0'
  spec.files = Dir['lib/**/*']
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport'
end
