module Scripts
  root_dir = ::Dir.pwd
  gem_list = %w[
    midnight-business_logic
    midnight-utilities
    midnight-rails
  ]

  class VersionPropagator
    def initialize(
      gem_list:,
      version_file:,
      **_
    )
      @gem_list = gem_list
      @version_file = version_file
      freeze
    end

    def propagate(version)
      @gem_list.each do |gem_name|
        version_rb = @version_file.load(gem_name)
        current_version_rb = version_rb.sub(
          /(^\s*VERSION\s*=\s*)\S+(\s*?$)/,
          "\\1'#{version}'\\2"
        )
        @version_file.save(gem_name, current_version_rb)
      end
    end
  end

  class VersionFile
    def initialize(
      root_path:,
      **_
    )
      @root_path = root_path
      freeze
    end

    def save(gem_name, content)
      ::File.write(version_file_path(gem_name), content)
    end

    def load(gem_name)
      ::File.read(version_file_path(gem_name))
    end

    private

    def lib_path(gem_name)
      "lib/#{gem_name.gsub(/-/, '/')}"
    end

    def version_file_path(gem_name)
      "#{@root_path}/#{gem_name}/#{lib_path(gem_name)}/version.rb"
    end
  end

  populator = VersionPropagator.new(
    gem_list: gem_list,
    version_file: VersionFile.new(
      root_path: root_dir
    ))
  populator.propagate(::File.read(
    "#{root_dir}/GEM_VERSION"
  ))
end
