require 'active_record'
require 'midnight/rails'

module Midnight
  module Rails
    module Support
      class TestMigration < ::ActiveRecord::Migration::Current
        include DefaultMigration

        def change
          change_state_table
          change_event_table
        end
      end

      module Database
        module_function def setup
          disabled_logger do
            ::ActiveRecord::Base.establish_connection(
              adapter: 'sqlite3',
              database: ':memory:'
            )
            TestMigration.migrate(:up)
          end
        end

        module_function def tear_down
          disabled_logger do
            TestMigration.migrate(:down)
          end
        end

        private

        module_function def disabled_logger(&block)
          ::ActiveRecord::Migration.suppress_messages(&block)
        end
      end
    end
  end
end
