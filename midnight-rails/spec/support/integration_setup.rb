require 'active_record'
require 'active_support/concern'
require_relative './database'

module Midnight
  module Rails
    module Support
      module IntegrationSetup
        extend ::ActiveSupport::Concern

        included do
          around(:each) do |example|
            Database.setup
            example.run
          ensure
            begin
              Database.tear_down
            rescue ::ActiveRecord::StatementInvalid
              nil
            end
          end
        end
      end
    end
  end
end
