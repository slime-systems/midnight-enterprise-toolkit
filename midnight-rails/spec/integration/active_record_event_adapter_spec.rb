require_relative '../support/integration_setup'

module Midnight
  module Rails
    module ActiveRecordEventAdapterTest
      class Sent < DryEvent
        class << self
          def data_schema
            ::Dry::Schema.Params
          end
        end
      end

      RSpec.describe ActiveRecordEventAdapter do
        include Support::IntegrationSetup

        subject do
          described_class.new
        end

        it 'should be able to save a batch of events without error' do
          sent_event = Sent.new(
            data: {
              title: 'Holy cow!',
              description: 'It works.',
            }
          )
          expect do
            test_events = 4.times.map {sent_event}
            subject.save_events(test_events, next_position: 1, state_id: 100)
          end.not_to raise_error
          event_active_record = subject.active_record
          last_record = event_active_record.last
          expect(last_record.state_id).to eq(100)
          expect(last_record.position).to eq(4)
          expect(last_record.data).to be_include('Holy')
          expect(event_active_record.count).to eq(4)
        end
      end
    end
  end
end
