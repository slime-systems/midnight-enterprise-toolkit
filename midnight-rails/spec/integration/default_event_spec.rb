require 'activerecord-import'
require_relative '../support/integration_setup'

module Midnight
  module Rails
    module DefaultEventTest
      RSpec.describe DefaultEvent do
        include Support::IntegrationSetup

        subject do
          described_class.new
        end

        it 'should declare essential fields for event persistence operations' do
          expect(subject).to be_respond_to(:data)
          expect(subject).to be_respond_to(:metadata)
          expect(subject).to be_respond_to(:position)
          expect(subject).to be_respond_to(:occurred_at)
        end


        it 'should be monkey-patchable by ActiveRecord::Import' do
          expect(described_class).to be_respond_to(:import)
        end
      end
    end
  end
end
