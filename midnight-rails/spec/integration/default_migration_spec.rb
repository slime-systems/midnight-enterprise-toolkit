require_relative '../support/database'

module Midnight
  module Rails
    module DefaultMigrationTest
      RSpec.describe DefaultMigration do
        it 'should be able to migrate the database under the active record' do
          expect do
            Support::Database.setup
            DefaultState.new
            DefaultEvent.new
            Support::Database.tear_down
          end.not_to raise_error
        end
      end
    end
  end
end
