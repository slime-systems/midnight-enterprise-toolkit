require 'active_record'
require 'securerandom'
require_relative '../support/integration_setup'

module Midnight
  module Rails
    module DefaultStateTest
      RSpec.describe DefaultState do
        include Support::IntegrationSetup

        subject do
          described_class.new
        end

        it 'should declare essential fields for the event operations' do
          expect(subject).to be_respond_to(:state)
          expect(subject).to be_respond_to(:aggregate_key)
          expect(subject).to be_respond_to(:next_position)
        end

        it 'should not allow duplication of the same aggregate' do
          expect do
            2.times do
              described_class.new(
                aggregate_key: 'a'
              ).save!
            end
          end.to raise_error(::ActiveRecord::RecordNotUnique)
        end

        it 'should be able to detect concurrent write' do
          original_record = described_class.create!(
            aggregate_key: 'a'
          )
          instances = 2.times.map do
            described_class.find(original_record.id)
          end
          expect do
            instances.each do |instance|
              instance.state = ::SecureRandom.uuid
              instance.save!
            end
          end.to raise_error(::ActiveRecord::StaleObjectError)
        end
      end
    end
  end
end
