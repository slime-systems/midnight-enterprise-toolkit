require_relative '../support/integration_setup'

module Midnight
  module Rails
    module StateAdapterTest
      RSpec.describe StateAdapter do
        include Support::IntegrationSetup

        subject do
          described_class.new(
            active_record: DefaultState
          )
        end

        it 'should be able to load a non-existing state' do
          state_holder = subject.load(
            key: 'TestAggregate/1'
          )
          expect(state_holder).not_to be_nil
          expect(state_holder.state).to be_nil
        end

        context do
          let(:load_state_holder) do
            lambda do
              subject.load(
                key: 'TestAggregate/1'
              )
            end
          end
          let(:save_state_holder) do
            lambda do |test_data|
              state_holder = load_state_holder.call
              state_holder.state = {
                data: test_data
              }
              state_holder.save!
            end
          end

          it 'should be able to persist the state' do
            2.times do |number|
              test_data = "a#{number}"
              save_state_holder.call(test_data)
              state_holder = load_state_holder.call
              expect(state_holder.state[:data]).to eq(test_data)
            end
          end

          it 'should be able to detect concurrent writes' do
            holder_list = 2.times.map do |number|
              holder = load_state_holder.call
              holder.state = {
                data: number
              }
              holder
            end.to_a
            expect do
              holder_list.each(&:save!)
            end.to raise_error(Commons::ConcurrentWriteError)
          end

          it 'should be able to wrap an execution in a transaction' do
            save_state_holder.call('original_data')
            subject.transaction do
              save_state_holder.call('modified_data')
              raise StandardError
            end rescue nil
            state_holder = load_state_holder.call
            expect(state_holder.state[:data]).to eq('original_data')
          end

          it 'should execute the commit hook when the transaction succeeded' do
            test = 0
            subject.transaction do
              subject.hook_commit do
                test += 1
              end
            end
            expect(test).to eq(1)
          end

          it 'should not execute the commit hook when the transaction rolled back' do
            test = 0
            subject.transaction do
              subject.hook_commit do
                test += 1
              end
              raise StandardError
            end rescue nil
            expect(test).to eq(0)
          end

          it 'should forward metadata from the underlying active record' do
            save_state_holder.call('some_data')
            state_holder = load_state_holder.call
            metadata = state_holder.metadata
            expect(metadata).to have_key(:next_position)
            expect(metadata).to have_key(:state_id)
          end
        end
      end
    end
  end
end
