require 'active_record'
require 'securerandom'
require_relative '../support/integration_setup'

module Midnight
  module Rails
    module InteractorIntegrationTest
      class SendCommand
        include Command
        attr_accessor \
          :title,
          :description
      end

      class DoNothingCommand
        include Command
      end

      class Sent < DryEvent
        class << self
          def data_schema
            ::Dry::Schema.Params
          end
        end
      end

      class NothingDone < DryEvent
        class << self
          def data_schema
            ::Dry::Schema.Params
          end
        end
      end

      AlreadySent = Class.new(StandardError)

      class Alert
        include BusinessLogic::AggregateRoot

        def initialize(\
          state: nil,
          user_id:
        )
          @state = state || {
            user_id: user_id,
            sent: false,
          }
        end

        def dispatch(command)
          case command
          when SendCommand
            send_alert(
              title: command.title.strip,
              description: command.description.strip
            )
          when DoNothingCommand
            do_nothing
          else
            # :nocov:
            super(command)
            # :nocov:
          end
        end

        def send_alert(title:, description:)
          raise AlreadySent if state[:sent]
          apply(
            Sent.strict(
              data: {
                user_id: state[:user_id],
                title: title,
                description: description
              }
            )
          )
        end

        def do_nothing
          apply(
            NothingDone.strict(data: {})
          )
        end

        on Sent do |_event|
          state[:sent] = true
        end
      end

      RSpec.describe ActiveRecordInteractor do
        include Support::IntegrationSetup

        let(:state_adapter) do
          StateAdapter.new(
            active_record: DefaultState
          )
        end
        let(:dispatch_a_command) do
          lambda do |interactor, multiplier: 1|
            commands = multiplier.times.map do
              SendCommand.new(
                title: 'Holy cow!',
                description: 'It works.'
              )
            end
            interactor.call(*commands)
          end
        end

        subject do
          lambda do
            described_class.new(
              aggregate_key: "#{Alert.name}/singleton",
              build_aggregate: lambda do |state:|
                Alert.new(
                  state: state,
                  user_id: 'singleton'
                )
              end
            )
          end
        end

        it 'should be able to dehydrate and hydrate the state from the database' do
          dispatch_a_command.call(subject.call)
          expect do
            dispatch_a_command.call(subject.call)
          end.to raise_error(AlreadySent)
        end

        it 'should advance the metadata of the aggregate' do
          dispatch_a_command.call(subject.call)
          state_holder = state_adapter.load(
            key: "#{Alert.name}/singleton",
          )
          expect(state_holder.metadata[:next_position]).to eq(2)
        end

        it 'should execute multiple commands in a single database transaction' do
          expect do
            dispatch_a_command.call(subject.call, multiplier: 2)
          end.to raise_error(AlreadySent)
          state_holder = state_adapter.load(
            key: "#{Alert.name}/singleton",
          )
          # the operation should entirely rolled back, hence next_position = 1
          expect(state_holder.metadata[:next_position]).to eq(1)
        end

        it 'should handle multiple events generated in a transaction correctly' do
          event_count = 3
          commands = event_count.times.map do
            DoNothingCommand.new
          end
          instance = subject.call
          instance.call(*commands)
          state_holder = state_adapter.load(
            key: "#{Alert.name}/singleton",
          )
          expect(state_holder.metadata[:next_position]).to eq(event_count + 1)
        end
      end
    end
  end
end
