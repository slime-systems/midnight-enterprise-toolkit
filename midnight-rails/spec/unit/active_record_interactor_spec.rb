require 'ostruct'
require 'active_support/core_ext/hash'

module Midnight
  module Rails
    module InteractorTest
      class SendCommand
        include Command
        attr_accessor \
          :title,
          :description
      end

      class Sent < DryEvent
        class << self
          def data_schema
            ::Dry::Schema.Params
          end
        end
      end

      AlreadySent = Class.new(StandardError)

      class Alert
        include BusinessLogic::AggregateRoot

        def initialize(\
          state: nil,
          user_id:
        )
          @state = state || {
            user_id: user_id,
            sent: false,
          }
        end

        def dispatch(command)
          case command
          when SendCommand
            send_alert(
              title: command.title.strip,
              description: command.description.strip
            )
          else
            # :nocov:
            super(command)
            # :nocov:
          end
        end

        def send_alert(title:, description:)
          raise AlreadySent if state[:sent]
          apply(
            Sent.strict(
              data: {
                user_id: state[:user_id],
                title: title,
                description: description
              }
            )
          )
        end
      end

      RSpec.describe ActiveRecordInteractor do
        context do
          let(:null_operation) { lambda { |*_args| } }
          let(:default_persistance_class) do
            Class.new do
              define_method(:transaction) do |&block|
                block.call
              end

              define_method(:hook_commit) do |&block|
                block.call
              end

              define_method(:load) do |*_args|
                OpenStruct.new
              end
            end
          end
          let(:send_command) do
            SendCommand.new(
              title: 'Title',
              description: 'Description'
            )
          end
          let(:build_interactor) do
            lambda do | \
              aggregate_key: 'Alert/1',
              build_aggregate: lambda do |state:|
                Alert.new(
                  user_id: '1',
                  state: state
                )
              end,
              state_persistence: default_persistance_class.new,
              transaction_handler: Commons::NULL_EVENT_HANDLER,
              committed_handler: Commons::NULL_EVENT_HANDLER,
              advance_state_metadata: null_operation,
              save_state: null_operation \
            |
              described_class.new(**{
                aggregate_key: aggregate_key,
                build_aggregate: build_aggregate,
                state_persistence: state_persistence,
                advance_state_metadata: advance_state_metadata,
                save_state: save_state,
                transaction_handler: transaction_handler,
                committed_handler: committed_handler,
              }.compact)
            end
          end

          it 'should load state in a transaction' do
            in_transaction = false
            expect_count = 0
            expect_inside_transaction = lambda do
              expect_count += 1
              expect(in_transaction).to be true
            end
            interactor = build_interactor.call(
              state_persistence: Class.new(default_persistance_class) do
                define_method(:transaction) do |&block|
                  in_transaction = true
                  super(&block)
                  in_transaction = false
                end

                define_method(:hook_commit) do |&block|
                  expect_inside_transaction.call
                  super(&block)
                end

                define_method(:load) do |args|
                  expect_inside_transaction.call
                  super(*args)
                end
              end.new
            )
            interactor.call(send_command)
            expect(expect_count).to eq(2)
          end

          it 'should dispatch events with the aggregate metadata' do
            transaction_args = nil
            committed_args = nil
            interactor = build_interactor.call(
              state_persistence: Class.new(default_persistance_class) do
                define_method(:load) do |*_args|
                  OpenStruct.new(
                    metadata: {
                      test: true
                    }
                  )
                end
              end.new,
              transaction_handler: lambda do |*args|
                transaction_args = args
              end,
              committed_handler: lambda do |*args|
                committed_args = args
              end,
            )
            interactor.call(send_command)
            [transaction_args, committed_args].each do |dispatched_args|
              expect(dispatched_args).not_to be_nil
              events, metadata = dispatched_args
              expect(events&.map(&:class)).to contain_exactly(Sent)
              expect(metadata[:test]).to be true
            end
          end

          it 'should return events generated inside the transaction' do
            interactor = build_interactor.call
            events = interactor.call(send_command)
            expect(events&.map(&:class)).to contain_exactly(Sent)
          end

          it 'should save the updated active record at the end of the transaction' do
            metadata_events = nil
            expect_metadata_advanced = lambda do
              expect(metadata_events).not_to be_nil
            end
            active_record_saved = false
            interactor = build_interactor.call(
              state_persistence: Class.new(default_persistance_class) do
                define_method(:load) do |*args|
                  record = super(*args)
                  record.define_singleton_method(:advance_metadata) do |events|
                    metadata_events = events
                  end
                  record.define_singleton_method(:save!) do
                    expect_metadata_advanced.call
                    active_record_saved = true
                  end
                  record
                end
              end.new,
              advance_state_metadata: nil, # use the default
              save_state: nil # use the default
            )
            interactor.call(send_command)
            expect_metadata_advanced.call
            expect(metadata_events&.map(&:class)).to contain_exactly(Sent)
            expect(active_record_saved).to be true
          end
        end
      end
    end
  end
end
