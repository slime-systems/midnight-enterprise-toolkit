module Midnight
  module Rails
    module CommandTest
      class SendCommand
        include Command
        attr_accessor \
          :title,
          :description

        validates :title, presence: true
      end

      RSpec.describe Command do
        context do
          it 'should be able to validate fields with active model validations' do
            expect do
              SendCommand.new.validate!
            end.to raise_error(::ActiveModel::ValidationError)
            expect do
              SendCommand.new(title: 'Holy cow!').validate!
            end.not_to raise_error
          end

          it 'should be able to hold the arguments supplied' do
            description = 'some description'
            command = SendCommand.new(
              description: description
            )
            expect(command.description).to eq(description)
          end
        end
      end
    end
  end
end
