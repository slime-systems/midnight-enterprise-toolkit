require 'dry-schema'

module Midnight
  module Rails
    module DryEventTest
      class Sent < DryEvent
        class << self
          def data_schema
            ::Dry::Schema.Params do
              required(:title).filled(:string)
              required(:description).filled(:string)
            end
          end
        end
      end

      RSpec.describe DryEvent do
        context do
          it 'should prevent schema violations in strict mode' do
            expect do
              Sent.strict(
                data: {
                  title: nil
                }
              )
            end.to raise_error(BusinessLogic::Event::SchemaViolation)
            expect do
              Sent.strict(
                data: {
                  title: 'Holy cow!',
                  description: 'Something is gonna happen'
                }
              )
            end.not_to raise_error
          end

          it 'should allow initialization without validation' do
            expect do
              Sent.new(
                data: {
                  title: nil
                }
              )
            end.not_to raise_error
          end

          it 'should be able to hold the arguments supplied' do
            event = Sent.new(
              data: {
                title: :a,
              },
              metadata: {
                testing: :b,
              }
            )
            expect(event.data[:title]).to eq(:a)
            expect(event.metadata[:testing]).to eq(:b)
          end

          it 'should raise an error if no schema implemented' do
            expect do
              described_class.strict(data: {})
            end.to raise_error(NotImplementedError)
          end
        end
      end
    end
  end
end
