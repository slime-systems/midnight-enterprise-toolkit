# frozen_string_literal: true

version = File.read(File.expand_path('../GEM_VERSION', __dir__)).strip
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'midnight/rails/version'

Gem::Specification.new do |spec|
  spec.name = 'midnight-rails'
  spec.version = version
  spec.author = 'Sarun Rattanasiri'
  spec.email = 'midnight_w@gmx.tw'
  spec.license = 'BSD-3-Clause'

  spec.summary = 'The default integration of Midnight::BusinessLogic with the sane choices.'
  spec.description = 'Midnight::Rails enrich your Rails, and non-Rails application, '\
    'decouples your business logic from '\
    'frameworks, databases, and UI with the sane default. '\
    'See for yourself how far an application '\
    'adopting the clean architecture can go.'
  spec.homepage = 'https://gitlab.com/slime-systems/midnight-enterprise-toolkit/tree/master/midnight-rails'
  spec.metadata = {
    'source_code_uri' => "https://gitlab.com/slime-systems/midnight-enterprise-toolkit/tree/v#{version}/midnight-rails"
  }

  spec.required_ruby_version = '>= 2.5.0'
  spec.files = Dir['lib/**/*']
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport'
  spec.add_dependency 'activerecord'
  spec.add_dependency 'activerecord-import'
  spec.add_dependency 'activemodel'
  spec.add_dependency 'dry-schema'
  spec.add_dependency 'midnight-business_logic'
  spec.add_dependency 'midnight-utilities'
end
