# frozen_string_literal: true

require_relative 'rails/version'
require 'active_support/dependencies/autoload'

module Midnight
  module Rails
    extend ::ActiveSupport::Autoload

    autoload :ActiveRecordInteractor
    autoload :ActiveRecordWrapper
    autoload :CommitHooker
    autoload :StateAdapter
    autoload :DefaultState
    autoload :ActiveRecordEventAdapter
    autoload :DefaultEvent
    autoload :Command
    autoload :DryEvent
    autoload :DefaultMigration
    autoload :StaleObjectError
  end
end
