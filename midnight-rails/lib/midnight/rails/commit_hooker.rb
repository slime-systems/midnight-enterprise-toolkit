# frozen_string_literal: true

module Midnight
  module Rails
    # after_commit hook
    # https://dev.to/evilmartians/rails-aftercommit-everywhere--4j9g
    class CommitHooker
      def initialize(&after_commit)
        @after_commit = after_commit
        freeze
      end

      # :nocov:
      # rubocop:disable Style/MethodMissingSuper
      def method_missing(method_name, *_args)
        after_commit.call if method_name == :committed!
      end
      # rubocop:enable Style/MethodMissingSuper

      def respond_to_missing?(_method_name, _include_private = false)
        true
      end
      # :nocov:

      private

      attr_reader :after_commit
    end
  end
end
