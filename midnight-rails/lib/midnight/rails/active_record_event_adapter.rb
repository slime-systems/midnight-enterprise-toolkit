# frozen_string_literal: true

require 'activerecord-import'
require 'midnight/utilities'
require 'time'

# TODO: remove this once the upstream releases the fix
# https://github.com/zdennis/activerecord-import/commit/85c69185469e195f05d575c70b37a9fbf47f7bc3
require 'active_support/core_ext/array'

module Midnight
  module Rails
    # noinspection RubyDefParenthesesInspection
    # noinspection RubyParameterNamingConvention
    class ActiveRecordEventAdapter
      attr_reader :active_record

      def initialize(\
        active_record: DefaultEvent,
        serializer: Utilities::JsonSerializer,
        fetch_event_timestamp: lambda do |event|
          ::Time.parse(event.metadata[:occurred_at])
        end
      )
        @active_record = active_record
        @serializer = serializer
        @fetch_event_timestamp = fetch_event_timestamp
        freeze
      end

      def save_events(events = [], next_position:, state_id:, **_)
        serialized_events = events.each_with_index.map do |unserialized_event, position_offset|
          {
            event_type: unserialized_event.class.name,
            data: @serializer.dump(unserialized_event.data),
            metadata: @serializer.dump(unserialized_event.metadata),
            position: next_position + position_offset,
            state_id: state_id,
            occurred_at: @fetch_event_timestamp.call(unserialized_event),
          }
        end
        # https://github.com/zdennis/activerecord-import
        active_record.import(serialized_events, validate: false)
      end

      # load functionality intentionally omitted
      # different use cases requires different load implementation
      # so we don't include it in Midnight::Rails
      # but you can implement one easily

    end
  end
end
