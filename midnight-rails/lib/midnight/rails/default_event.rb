# frozen_string_literal: true

module Midnight
  module Rails
    class DefaultEvent < ::ActiveRecord::Base
      self.table_name = 'midnight__events'
    end
  end
end
