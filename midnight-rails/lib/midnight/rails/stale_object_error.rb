# frozen_string_literal: true

require 'active_record'
require 'midnight/business_logic'

module Midnight
  module Rails
    class StaleObjectError < ::ActiveRecord::StaleObjectError
      include Commons::ConcurrentWriteError
    end
  end
end
