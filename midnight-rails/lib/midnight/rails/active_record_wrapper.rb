# frozen_string_literal: true

require 'active_record'
require 'midnight/business_logic'

module Midnight
  module Rails
    class ActiveRecordWrapper

      def initialize(
        record,
        serializer:,
        metadata_adapter:,
        advance_metadata:
      )
        @record = record
        @serializer = serializer
        @metadata_adapter = metadata_adapter
        @advance_metadata = advance_metadata
        @state_loaded = false
        @state_changed = false
      end

      def metadata
        @metadata_adapter.call(@record)
      end

      def state
        return @changed_state if @state_changed
        return @loaded_state if @state_loaded
        @loaded_state = @serializer.load(@record.state)
        @state_loaded = true
        @loaded_state
      end

      def state=(new_state)
        @state_changed = true
        @changed_state = new_state
      end

      def advance_metadata(events)
        @advance_metadata.call(@record, events)
      end

      def save!
        @record.state = @serializer.dump(@changed_state) \
          if @state_changed
        @record.save!
      rescue ::ActiveRecord::StaleObjectError => e
        # rescue just to add the convenient marker module to the exception
        raise e if e.is_a?(Commons::ConcurrentWriteError)
        raise StaleObjectError.new(e.record, e.attempted_action)
      end
    end
    private_constant :ActiveRecordWrapper
  end
end
