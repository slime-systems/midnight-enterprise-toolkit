# frozen_string_literal: true

require 'dry/schema'
require 'midnight/business_logic'

module Midnight
  module Rails
    class DryEvent < BusinessLogic::Event
      class SchemaViolation < ::ArgumentError
        include BusinessLogic::Event::SchemaViolation

        def initialize(validation_result, message = nil)
          super(message)
          @validation_result = validation_result
        end

        def errors
          @error_details ||= @validation_result.errors.to_h
        end
      end

      class << self
        private

        def ensure_schema(data)
          @data_schema ||= data_schema
          result = @data_schema.call(data)
          raise SchemaViolation.new(result) unless result.success?
          result.to_h
        end

        def data_schema
          raise NotImplementedError
          # example
          # ::Dry::Schema.Params do
          #   required(:example_field).filled(:string)
          # end
        end
      end
    end
  end
end
