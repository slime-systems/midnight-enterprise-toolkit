# frozen_string_literal: true

require 'active_support/concern'
require 'active_model'

module Midnight
  module Rails
    module Command
      extend ::ActiveSupport::Concern

      include ::ActiveModel::Model
      include ::ActiveModel::Attributes
    end
  end
end
