# frozen_string_literal: true

require 'active_record'
require 'midnight/utilities'

module Midnight
  module Rails
    class StateAdapter
      def initialize(\
        active_record: DefaultState,
        serializer: Utilities::JsonSerializer,
        metadata_adapter: lambda do |record|
          {
            next_position: record.next_position,
            state_id: record.id,
            aggregate_key: record.aggregate_key,
          }
        end,
        advance_metadata: lambda do |record, events|
          record.next_position += events.length
        end
      )
        @model = active_record
        @serializer = serializer
        @metadata_adapter = metadata_adapter
        @advance_metadata = advance_metadata
      end

      def load(key:, **_)
        record = begin
          @model.where(
            aggregate_key: key
          ).first_or_create!
        rescue ::ActiveRecord::RecordNotUnique
          # :nocov:
          retry
          # :nocov:
        end
        ActiveRecordWrapper.new(
          record,
          serializer: @serializer,
          metadata_adapter: @metadata_adapter,
          advance_metadata: @advance_metadata
        )
      end

      def transaction(&block)
        @model.transaction(&block)
      end

      def hook_commit(&block)
        @model.connection.add_transaction_record(
          CommitHooker.new(&block)
        )
      end
    end
  end
end
