# frozen_string_literal: true

module Midnight
  module Rails
    class DefaultState < ::ActiveRecord::Base
      self.table_name = 'midnight__states'
    end
  end
end
