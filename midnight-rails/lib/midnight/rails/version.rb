# frozen_string_literal: true

module Midnight
  module Rails
    VERSION = '0.0.4'
  end
end
