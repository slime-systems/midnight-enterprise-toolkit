# Midnight Enterprise Toolkit

## What is it?
It is a collection of Ruby libraries extracted from experiences
of a software architect working with large enterprises.  
The collection comprises of
- [__Midnight::BusinessLogic__](midnight-business_logic) let you write a clean business logic
agnostic to frameworks (e.g., Rails, Hanami, Trailblazer),
user interface (e.g., Console, HTML, CSS),
API format and transport (e.g., XML, JSON, GraphQL, HTTP, WebSocket),
and data storage (e.g., MySQL, MongoDB, Elasticsearch).
Leaving all possibilities open without compromise.
- [__Midnight::Rails__](midnight-rails) a default and recommended integration of `Midnight::BusinessLogic`,
comes with sane choices, including the integrations of `dry-schema`, and `Active Record`.
While still leaving the door open for more extensions and integrations
(e.g., Using MongoDB as the primary storage instead of relational databases).
Despite its name, it does not depends on the whole Rails framework.
You can use it on Rails and non-Rails application, or opting-out the `Active Record` entirely.
- [__Midnight::Utilities__](midnight-utilities) a collection of artifacts extracted from `Midnight::Rails`
that potentially useful somewhere else.

## What's wrong with enterprises and Ruby?
Have you ever wonder why enterprises (think: banking) don't use Ruby on Rails in general.  
I asked the very same question in the past, and with my egotism, I used Rails in an enterprise environment.  
The result? Catastrophic failure; and now I can tell you why.  
It is not Ruby's fault, it is partially Rails' fault, but ultimately it is my fault,
in general sense, it is the fault of people from the Ruby community.  
  
Everyone who learned Rails think, at some point in their career, that everything can be abstracted to CRUD operations.
"Sign In" can be viewed as "Create Session", or another way to say "Sign Up" is to "Create User".

But, as you progress in your career, you will eventually understand that you can not do that to everything,
or if you are dogmatic you can call operations whatever you like,
but it will be at the astronomical cost. The abstraction is leaky.

If you experienced it already, you are not alone. The phenomenon has a name. It is "[object-relational impedance mismatch](https://en.wikipedia.org/wiki/Object-relational_impedance_mismatch)".  
But, if not, I can only tell you that the problem is real.  
I cannot convince you any further, but if you live with Rails long enough, you will experience the same thing yourself.

If you try using Rails in an enterprisey organization, at some point, you can't be dogmatic and push Rails' philosophy any further.  
If at this point you don't change, you are the problem yourself. And if a lot of Rails developers doing this.
Enterprises rather don't hire Rails developers in the first place.
And that's where they split parted way.

You can choose how you want to use Rails. You can be picky and use Rails for what it good at.  
But, if you can get out of your comfort zone, throwing away Rails as a philosophy, but still keep using it as a high-quality tool,
you will see how beautiful, both Ruby and Rails can be.

## Entering "Midnight Enterprise Toolkit"
This toolkit offers you lightweight but powerful tools as a backbone of your project.

If the first thing you do in your project is designing
[entity-relationship diagram](https://en.wikipedia.org/wiki/Entity%E2%80%93relationship_model),
this toolkit will __set you free from the database__. You can think about the database later.
You can stop designing the diagram, and stop thinking where to put indexes, right away.

If the first thing you do in your project is designing
[UX](https://en.wikipedia.org/wiki/User_experience)/[UI](https://en.wikipedia.org/wiki/User_interface),
no [wireframe](https://en.wikipedia.org/wiki/Website_wireframe) no coding,
this toolkit will also __set you free from all visual designs__. You can think about them later.

What else left to think about? __The business logic__, the most important one, but largely neglected,
because everyone is too busy doing something else less important.

The business logic should be agnostic to everything else.
The very first line of code in a project you put some thought into
should be a part of the business logic of your application.

In other words:  
Start the project with the business logic;
everything else is less important and can be figured out.

The toolkit enables you to do just that.
Focus on the business logic, knowing that everything else will be taken care of.

## No discrimination

__Midnight Enterprise Toolkit__ respects your choices, not just in words, but with the code.  
It can be used on top of any Ruby framework.  
It allows you to do an "event sourcing" if you want, but at the same time,
it won't push "event-sourcing" ideology on anyone.

It works with many more databases than the Active Record.
It even enables people to use MongoDB safely in the banking business.  
It won't take away your choice of database scaling with sharding.  
It won't take away your choice to implement hash chain ensuring data integrity.

You can pick only the good parts of Domain-Driven Design (DDD),
and mixed it with other powerful methodology in your arsenal,
your choice. The toolkit never assumed that you use DDD at all;
it just borrows some terminology from the DDD community.

The toolkit is carefully engineered to plays well with the community,
and highly extensible without touching or changing what exists.  

## What's next

Please have a look at the `README` of each gem for more details of how to use the libraries.

## Maintainer
- Sarun Rattanasiri (
[GitLab](https://gitlab.com/midnight-wonderer),
[GitHub](https://github.com/midnight-wonderer),
midnight_w\[a]gmx\[.]tw
)

## Plug
The project has been sponsored by [Slime Systems](https://slime.systems/).  
We are software consultants and API vendor
if you want to outsource parts of your project with confidence,
consider hiring us as your API vendor.

## License
Midnight Enterprise Toolkit is released under the [3-clause BSD License](LICENSE.md).
