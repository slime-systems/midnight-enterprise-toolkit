SHELL=/usr/bin/env bash

start: up

.PHONY: build up down bash

build:
	docker-compose build --pull application

down:
	docker-compose down

up:
	docker-compose up application

bash:
	(docker-compose run --rm application bash) || true

.PHONY: populate-gem-version build-gem release-gem

populate-gem-version:
	docker-compose run --rm application ruby ./scripts/populate_gem_version.rb

build-gem:
	(cd ./midnight-business_logic && bundle exec rake build) && \
	(cd ./midnight-utilities && bundle exec rake build) && \
	(cd ./midnight-rails && bundle exec rake build)

release-gem: build-gem
	(cd ./midnight-business_logic && bundle exec rake release) && \
	(cd ./midnight-utilities && bundle exec rake release) && \
	(cd ./midnight-rails && bundle exec rake release)
